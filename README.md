# SnowLeopardPlusPlus

Snow Leopard++ : A snow leopard theme for Pop Shell

## Install :
----
Make sure you have gnome tweak tools installed, and user themes extension enabled.

Then, just save `SL++` to `/home/user/.themes/`, and select the theme in User Themes.
## Todo :
----
- [x] Aqua controls
- [x] Aqua slider
  
  - ~~[ ] Slider always on~~ 

- [x] Silver shell
  
  - [X] Switch SVGs
  - [x] Calendar colors
  - [x] Weather colors

- [X] Progress bar
- [X] Checkbox / Radiobox
  - [ ] SVG for graphics
- [X] Switch
- [X] Licensing

## Screenshot :
----

![Latest screenshot](https://codeberg.org/aaruni96/SnowLeopardPlusPlus/raw/branch/master/screenshot.png)

## Credits :
----


Uses bits and pieces from the followings works :
 - [Pop!_OS System theme](https://github.com/pop-os/gtk-themes)
 - [B00merang-Project : OS-X-Leopard](https://github.com/B00merang-Project/OS-X-Leopard)
 - [Cameo-OSX](https://github.com/paullinuxthemer/cameo/tree/master/Cameo-OSX)